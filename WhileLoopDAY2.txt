package whilelLoopDAY2;

public class WhileLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      int i=123,rev=0 ,a ;
      System.out.println("the Original number is :"+i);
      while(i>0)
      {
    	  a=i%10;
    	  i= i/10;
    	  rev= rev*10+a;
      }
      System.out.println("Reverse of Number :"+ rev);
	}

}

OUTPUT : 

the Original number is :123
Reverse of Number :321
